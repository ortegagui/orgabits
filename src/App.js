import './App.scss';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'

import Menu from './layouts/Menu/Menu'
import Vegetables from './views/Vegetables/Vegetables'
import Fruits from './views/Fruits/Fruits'

function App() {
  return (
    <div className="app">
      <Router>
        <Menu />
        <Switch>
          <Route path="/fruits" component={Fruits} />
          <Route path="/vegetables" component={Vegetables} />

          <Route exact path="/"><Redirect to="/fruits" /></Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
