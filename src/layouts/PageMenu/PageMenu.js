import PageMenuItem from '../../components/PageMenuItem/PageMenuItem';
import './PageMenu.scss';
import twitter from '../../assets/icons/twitter.png'
import youtube from '../../assets/icons/youtube.png'
import instagram from '../../assets/icons/instagram.png'
import facebook from '../../assets/icons/facebook.png'

function PageMenu() {
    return (
        <div className="page-menu">
            <div id="our-products">
                <PageMenuItem child="Our products" />
            </div>
            <div className="page-menu-list">
                <PageMenuItem child="Pineapples" />
                <PageMenuItem child="Strawberries" />
                <PageMenuItem child="Apples" />
                <PageMenuItem child="Citrus" />
            </div>
            <div className="page-menu-list">
                <PageMenuItem child="Cauliflowers" />
                <PageMenuItem child="Tomatoes" />
                <PageMenuItem child="Eggplants" />
                <PageMenuItem child="Carotts" />
            </div>
            <div className="page-menu-list">
                <PageMenuItem nodash child={<img src={twitter} alt="" />} />
                <PageMenuItem nodash child={<img src={youtube} alt="" />} />
                <PageMenuItem nodash child={<img src={instagram} alt="" />} />
                <PageMenuItem nodash child={<img src={facebook} alt="" />} />
            </div>
        </div>
    );
}

export default PageMenu;
