import './Fruits.scss';
import PageMenu from '../../layouts/PageMenu/PageMenu'
import PageContent from '../../layouts/PageContent/PageContent'
import pineapple from '../../assets/img/pineapple.png'

function Fruits() {
    return (
        <div class="content" style={{ backgroundImage: `url(${pineapple})` }}>
            <PageMenu />
            <PageContent />
        </div>
    );
}

export default Fruits;
