import './Dash.scss';

function Dash(props) {
    const { orientation } = props
    if (orientation === "vertical") {
        return <span className="dash-vertical"></span>
    } else {
        return <span className="dash-horizontal"></span>
    }
}

export default Dash;