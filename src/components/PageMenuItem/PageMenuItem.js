import Dash from '../Dash/Dash';
import './PageMenuItem.scss';

function PageMenuItem(props) {
    const { nodash, child } = props

    if (nodash) {
        return (<div className="page-menu-item"><span className="child">{child}</span></div>)
    }

    return (
        <div className="page-menu-item">
            <Dash />
            <span className="child">{child}</span>
        </div>
    )
}

export default PageMenuItem;